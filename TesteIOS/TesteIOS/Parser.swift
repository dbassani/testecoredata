//
//  Parser.swift
//  TesteIOS
//
//  Created by JEGUDIEL on 09/11/16.
//  Copyright © 2016 Diego Bassani de Souza. All rights reserved.
//

import Foundation
import UIKit
import CoreData
class Parser {
    // Parser JSON
    class func parserPhotoArray(data: NSData) -> Array<PhotoBean> {
        
        var carros : Array<PhotoBean> = []
        
        do {
            let dict = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
            let photos: NSDictionary = dict["photos"] as! NSDictionary
            
            let arrayPhoto: NSArray = photos["photo"] as! NSArray
            
            for obj:AnyObject in arrayPhoto {
                let dict = obj as! NSDictionary
                let photoBean = parsePhoto(dict)
                
                carros.append(photoBean)
            }
        } catch let error as NSError {
            print("Erro ao ler JSON \(error)")
        }
        return carros
    }
    
    
    class func parsePhoto(data: NSDictionary) -> PhotoBean {

//        person.setValue(name, forKey: "name")
        let pb : PhotoBean = PhotoBean()
        let title:String = data["title"] as! String
        let id:String = data["id"] as! String
        let url_s:String = data["url_s"] as! String

        let dictDescription: NSDictionary = data["description"] as! NSDictionary
        let desc:String = dictDescription["_content"] as! String
        pb.id = id
        pb.title = title
        pb.desc = desc
        pb.url_s = url_s
        return pb
    }
}