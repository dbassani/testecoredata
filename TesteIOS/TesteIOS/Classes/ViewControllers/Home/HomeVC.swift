//
//  HomeVC.swift
//  TesteIOS
//
//  Created by JEGUDIEL on 09/11/16.
//  Copyright © 2016 Diego Bassani de Souza. All rights reserved.
//

import UIKit

class HomeVC: UIViewController, UITableViewDataSource,UITableViewDelegate {

    var arrayPhoto: Array<PhotoBean> = []
    var messageView:UIView!
    var page:Int = 1
    var coreData:PhotoCoreData = PhotoCoreData()

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        

        
        
        
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        // Para o scroll começar na posição do TableView
        self.automaticallyAdjustsScrollViewInsets = false;
        
        // Registra o CarroCell.xib como UITableViewCell da tabela
        let xib = UINib(nibName: "HomeCell", bundle:nil)
        self.tableView.registerNib(xib, forCellReuseIdentifier: "cell")
        
        
        if( Singleton.sharedInstance.isConnectedToNetwork()){
            self.initPhotos()
        }else{
            var array:Array<Photo> = coreData.getPhotoArray("teste")
            for item in array {
                let photoBean:PhotoBean = PhotoBean()
                photoBean.desc = item.desc!
                photoBean.id = item.id!
                photoBean.title = item.title!
                photoBean.url_s = item.url_s!
                arrayPhoto.append(photoBean)
                tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initPhotos() {
        let funcaoRetorno = { (array: Array<PhotoBean>!, error: NSError!) -> Void in
            self.messageView.removeFromSuperview()

            if(error != nil) {
                //Alerta.alerta("Erro: " + error.localizedDescription, viewController: self)
            } else {
                self.arrayPhoto = array
                for item in array {
                    var photoItem:Photo = PhotoCoreData.newInstance()
                    photoItem.desc = item.desc
                    photoItem.id = item.id
                    photoItem.title = item.title
                    photoItem.url_s = item.url_s
                    self.coreData.save(photoItem)
                }
                self.tableView.reloadData()
            }
        }
        messageView = Singleton.sharedInstance.progressBarDisplayer(self.view, msg: "Carregando", true)
        Service.getPhotos(String(page), paramTag: "flower", callback: funcaoRetorno)
    }

    
    //MARK: TABLEVIEW
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Se a variável opcional está inicializada, retorna a quantidade de carros
        return arrayPhoto.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Cria a célula desta linha
        let cell = self.tableView.dequeueReusableCellWithIdentifier("cell") as! HomeCell
        
        let linha = indexPath.row
        
        // Objeto do tipo carro
        let item = arrayPhoto[linha]
        cell.img.setUrl(item.url_s)

        cell.lbl.text = item.title
        //cell.lblName.text = item.area
        //cell.lblNome.text = genero.name
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let linha = indexPath.row
        
//        let especialidade = arrayEspecialidade[linha]
//        
//        let menuVC = DataHoraVC(nibName: "DataHoraVC", bundle: nil)
//        menuVC.especialidade = especialidade
//        self.navigationController!.pushViewController(menuVC, animated: true)

    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        var height = scrollView.frame.size.height;
        var contentYoffset = scrollView.contentOffset.y * 1.1;
        var distanceFromBottom = scrollView.contentSize.height - contentYoffset;
        if(distanceFromBottom < height)
        {
            let funcaoRetorno = { (array: Array<PhotoBean>!, error: NSError!) -> Void in
                if(error != nil) {
                    //Alerta.alerta("Erro: " + error.localizedDescription, viewController: self)
                } else {
                    self.arrayPhoto.appendContentsOf(array)
                    self.tableView.reloadData()
                }
            }
            page = page + 1
            //messageView = Singleton.sharedInstance.progressBarDisplayer(self.view, msg: "Carregando", true)
            if( Singleton.sharedInstance.isConnectedToNetwork()){
                Service.getPhotos(String(page), paramTag: "flower", callback: funcaoRetorno)
            }

        }
    }

}
