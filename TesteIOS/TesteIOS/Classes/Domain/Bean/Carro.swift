//
//  Carro.swift
//  TesteIOS
//
//  Created by JEGUDIEL on 09/11/16.
//  Copyright © 2016 Diego Bassani de Souza. All rights reserved.
//

import Foundation
import CoreData
class Carro : NSManagedObject {
    // id do banco de dados
    var id: Int = 0
    
    // tipo: clássico, esportivo, luxo
    var tipo: String = ""
    
    var nome: String = ""
    var desc: String = ""

    // Url para a foto do carro
    var url_foto: String = ""

    // Url com um site com informações do carro
    var url_info : String = ""

    // Url com o vídeo do carro
    var url_video : String = ""

    // Coordenadas da fábrica ou país de origem do carro
    var latitude : String = ""
    var longitude : String = ""
    @NSManaged var title: String!
    @NSManaged var url_s: String!
}
