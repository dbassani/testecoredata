//
//  Photo+CoreDataProperties.swift
//  TesteIOS
//
//  Created by JEGUDIEL on 09/11/16.
//  Copyright © 2016 Diego Bassani de Souza. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Photo {

    @NSManaged var desc: String?
    @NSManaged var url_s: String?
    @NSManaged var title: String?
    @NSManaged var id: String?

}
