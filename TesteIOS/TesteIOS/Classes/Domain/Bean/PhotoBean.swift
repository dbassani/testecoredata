//
//  PhotoBean.swift
//  TesteIOS
//
//  Created by JEGUDIEL on 09/11/16.
//  Copyright © 2016 Diego Bassani de Souza. All rights reserved.
//

import Foundation
class PhotoBean {
    var desc: String = ""
    var url_s: String = ""
    var title: String = ""
    var id: String = ""
}