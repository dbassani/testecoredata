//
//  CarroService.swift
//  TesteIOS
//
//  Created by JEGUDIEL on 09/11/16.
//  Copyright © 2016 Diego Bassani de Souza. All rights reserved.
//

import Foundation

class CarroService {
    
    class func getCarrosByTipo(tipo: String, callback: (carros:Array<Carro>!, error:NSError!) -> Void) {
        
        let http = NSURLSession.sharedSession()
        let url = NSURL(string:"http://www.livroiphone.com.br/carros/carros_"+tipo+".json")
        print(url)
        if(url != nil) {
            let task = http.dataTaskWithURL(url!, completionHandler: {
                (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
                
                if(error != nil) {
                    callback(carros:nil, error: error)
                } else {
                    
                    let carros = CarroService.parserJson(data!)
                    
                    dispatch_sync(dispatch_get_main_queue(), {
                        callback(carros:carros, error: nil)
                    })
                }
                
            })
            
            task.resume()
        }
    }
    
    // Busca por os carros pelo tipo: esportivos, clássicos ou luxo
    class func getCarrosByTipoFromFile(tipo: String) -> Array<Carro> {
        let file = "carros_" + tipo
        
        let path = NSBundle.mainBundle().pathForResource(file, ofType: "json")
        
        if(path == nil) {
            // Não encontrou o arquivo no projeto
            return [] // array vazio
        }
        
        // Conteúdo binário do arquivo
        let data = NSData(contentsOfFile: path!)
        
        if(data == nil) {
            // Não conseguiu ler o arquivo
            return [] // array vazio
        }
        
        if (data!.length == 0) {
            print("NSData vazio");
            return [] // array vazio
        }
        
        let carros = self.parserJson(data!)
        
        //let carros = parserXML_DOM(data!)
        
        for c in carros {
            // Atualiza o tipo do carro vamos precisar depois
            c.tipo = tipo
        }
        
        return carros
    }
    
    // Parser JSON
    class func parserJson(data: NSData) -> Array<Carro> {
        
        var carros : Array<Carro> = []
        
        // Faz a leitura do JSON, converte para dictionary
        do {
            let dict = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
            
            // Dictionary para todos os carros
            let jsonCarros: NSDictionary = dict["carros"] as! NSDictionary
            let arrayCarros: NSArray = jsonCarros["carro"] as! NSArray
            
            // Array de carros
            for obj:AnyObject in arrayCarros {
                let dict = obj as! NSDictionary
                let carro = Carro()
                carro.nome = dict["nome"] as! String
                carro.desc = dict["desc"] as! String
                carro.url_info = dict["url_info"] as! String
                carro.url_foto = dict["url_foto"] as! String
                carro.url_video = dict["url_video"] as! String
                carro.latitude = dict["latitude"] as! String
                carro.longitude = dict["longitude"] as! String
                carros.append(carro)
            }
        } catch let error as NSError {
            print("Erro ao ler JSON \(error)")
        }
        // Retorna a lista de carros
        return carros
    }

    // Retorna um array de carros de forma simples
    class func getCarros() -> Array<Carro> {
        var carros : Array<Carro> = []
        for (var i = 0; i < 20; i += 1) {
            let c = Carro()
            c.nome = "Ferrari " + String(i)
            c.desc = "Desc Carro " + String(i)
            c.url_foto = "Ferrari_FF.png"
            c.url_info = "http://www.google.com.br"
            
            // Adiciona o carro no array
            carros.append(c)
        }
        
        return carros
    }
}