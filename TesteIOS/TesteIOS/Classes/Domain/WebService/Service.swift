//
//  Service.swift
//  TesteIOS
//
//  Created by JEGUDIEL on 09/11/16.
//  Copyright © 2016 Diego Bassani de Souza. All rights reserved.
//

import Foundation

class Service {
    
    class func getPhotos(paramPage: String, paramTag: String, callback: (carros:Array<PhotoBean>!, error:NSError!) -> Void) {
        
        let http = NSURLSession.sharedSession()
        var url:String = ServicoParametro.SERVICO_GET_PHOTO
        url = (url as NSString).stringByReplacingOccurrencesOfString("PARAM_PAGE", withString: paramPage)
        url = (url as NSString).stringByReplacingOccurrencesOfString("PARAM_TAG", withString: paramTag)
        print(url)
        let urlWS = NSURL(string:url)
            let task = http.dataTaskWithURL(urlWS!, completionHandler: {
                (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
                if(error != nil) {
                    callback(carros:nil, error: error)
                } else {
                    let carros = Parser.parserPhotoArray(data!)
                    dispatch_sync(dispatch_get_main_queue(), {
                        callback(carros:carros, error: nil)
                    })
                }
            })
            task.resume()
    }
}