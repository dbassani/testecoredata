//
//  ServicoParametro.swift
//  TesteIOS
//
//  Created by JEGUDIEL on 09/11/16.
//  Copyright © 2016 Diego Bassani de Souza. All rights reserved.
//

import Foundation
class ServicoParametro {
    static let url: String = "https://api.flickr.com/services/rest/"
//    static let SERVICO_GET_PHOTO: String            = url + "?method=flickr.photos.search&api_key=2c2fe03cc43d41864febfadedd03d4ec&tags=PARAM_TAG&extras=description%2Ctitle%2C+url_s&per_page=10&page=PARAM_PAGE&format=json&nojsoncallback=1&auth_token=72157672687727023-67ae7ffefaf95b02&api_sig=7683dcbec31ffa4272d50b58cb652a16"
    static let SERVICO_GET_PHOTO: String            = url + "?method=flickr.photos.search&api_key=2c2fe03cc43d41864febfadedd03d4ec&tags=PARAM_TAG&extras=description%2Ctitle%2C+url_s&per_page=10&page=PARAM_PAGE&format=json&nojsoncallback=1&auth_token=72157672687727023-67ae7ffefaf95b02"
    static let SERVICO_API_SIG:String = "&api_sig=PARAM_SIG"
    
    static let PARAM1:String = "methodflickr.photos.searchapi_key2c2fe03cc43d41864febfadedd03d4ectagsPARAM_TAGextras=description%2Ctitle%2C+url_sper_page10pagePARAM_PAGEformatjsonnojsoncallback1auth_token72157672687727023-67ae7ffefaf95b02"
    
    static let PARAM2:String = "methodflickr.photos.searchapi_key2c2fe03cc43d41864febfadedd03d4ectagsPARAM_TAGextrasdescription%2Ctitle%2C+url_sper_page10pagePARAM_PAGEformatjsonnojsoncallback1auth_token72157672687727023-67ae7ffefaf95b02"
}