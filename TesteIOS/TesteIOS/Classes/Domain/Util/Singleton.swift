//
//  Singleton.swift
//  TesteIOS
//
//  Created by JEGUDIEL on 09/11/16.
//  Copyright © 2016 Diego Bassani de Souza. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

class Singleton{
    static let sharedInstance = Singleton()

    func progressBarDisplayer(mainView: UIView, msg:String, _ indicator:Bool ) -> UIView {
        var activityIndicator = UIActivityIndicatorView()
        var strLabel = UILabel()
        var messageFrame = UIView()
        //println(msg)
        strLabel = UILabel(frame: CGRect(x: mainView.frame.midX - 40, y: mainView.frame.midY - 25, width: 200, height: 50))
        strLabel.text = msg
        strLabel.textColor = UIColor.whiteColor()
        //messageFrame = UIView(frame: CGRect(x: mainView.frame.midX - 90, y: mainView.frame.midY - 25 , width: 180, height: 50))
        messageFrame = UIView(frame: CGRect(x: 0, y: 0 , width: mainView.frame.width, height: mainView.frame.height))
        
        messageFrame.layer.cornerRadius = 0
        messageFrame.backgroundColor = UIColor(white: 0, alpha: 0.7)
        if indicator {
            activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
            activityIndicator.frame = CGRect(x: mainView.frame.midX - 90, y: mainView.frame.midY - 25, width: 50, height: 50)
            activityIndicator.startAnimating()
            messageFrame.addSubview(activityIndicator)
        }
        messageFrame.addSubview(strLabel)
        mainView.addSubview(messageFrame)
        
        return messageFrame
    }
    
    func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    func md5(string string: String) -> String {
        var digest = [UInt8](count: Int(CC_MD5_DIGEST_LENGTH), repeatedValue: 0)
        if let data = string.dataUsingEncoding(NSUTF8StringEncoding) {
            CC_MD5(data.bytes, CC_LONG(data.length), &digest)
        }
        
        var digestHex = ""
        for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
            digestHex += String(format: "%02x", digest[index])
        }
        
        return digestHex
    }
    

}