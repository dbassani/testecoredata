//
//  StringUtils.swift
//  TesteIOS
//
//  Created by JEGUDIEL on 09/11/16.
//  Copyright © 2016 Diego Bassani de Souza. All rights reserved.
//

import Foundation

class StringUtils {
    
    class func toString(data: NSData!) -> String! {
        if(data == nil) {
            return nil
        }
        let s = NSString(data: data, encoding: NSUTF8StringEncoding)
        return s as! String
    }
    
    class func toNSData(s: String) -> NSData {
        let data = s.dataUsingEncoding(NSUTF8StringEncoding)
        return data!
    }
    
    class func toCString(s: String) -> UnsafePointer<Int8> {
        // cast to NSString
        // const char *
        let cstring = (s as NSString).UTF8String
        return cstring
    }
    
    class func trim(s: String) -> String {
        return s.stringByTrimmingCharactersInSet(.whitespaceAndNewlineCharacterSet())
    }
    
    class func isEmpty(s: String!) -> Bool {
        let count = StringUtils.count(s)
        return count == 0
    }
    
    class func count(s: String!) -> Int {
        if(s == nil) {
            return 0
        }
        let length = s.characters.count
        return length
    }
    
    class func replace(s: String, string: String, withString: String) -> String {
        return s.stringByReplacingOccurrencesOfString(string, withString: withString, options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
    
    class func substringWithRange(str: String, start: Int, end: Int) -> String
    {
        if (start < 0 || start > str.characters.count)
        {
            print("start index \(start) out of bounds")
            return ""
        }
        else if end < 0 || end > str.characters.count
        {
            print("end index \(end) out of bounds")
            return ""
        }
        let range = Range(start: str.startIndex.advancedBy(start), end: str.startIndex.advancedBy(end))
        return str.substringWithRange(range)
    }
    
    class func substringWithRange(str: String, start: Int, location: Int) -> String
    {
        if (start < 0 || start > str.characters.count)
        {
            print("start index \(start) out of bounds")
            return ""
        }
        else if location < 0 || start + location > str.characters.count
        {
            print("end index \(start + location) out of bounds")
            return ""
        }
        let range = Range(start: str.startIndex.advancedBy(start), end: str.startIndex.advancedBy(start + location))
        return str.substringWithRange(range)
    }
    
}