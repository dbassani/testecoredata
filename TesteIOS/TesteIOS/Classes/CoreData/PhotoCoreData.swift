//
//  PhotoCoreData.swift
//  TesteIOS
//
//  Created by JEGUDIEL on 09/11/16.
//  Copyright © 2016 Diego Bassani de Souza. All rights reserved.
//

import Foundation
import CoreData
import UIKit
class PhotoCoreData{
    
    class func newInstance () -> Photo {
        // AppDelegate da aplicação
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        // Context para salvar/deletar/buscar objetos
        let context = appDelegate.managedObjectContext
        let c = NSEntityDescription.insertNewObjectForEntityForName("Photo", inManagedObjectContext: context) as! Photo
        return c
    }
    
    func save(carro: Photo) {
        
        // AppDelegate da aplicação
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        // Context para salvar/deletar/buscar objetos
        let context = appDelegate.managedObjectContext
        
        // Seta o timestamp (como se fosse o id)
        //if (carro.timestamp == nil) {
        //carro.timestamp = NSDate()
        //}
        
        // Salva o carro
        var ok:Bool
        do {
            try context.save()
            ok = true
        } catch let saveError as NSError {
            print("Erro ao salvar: \(saveError.localizedDescription)")
            ok = false
        }
        
        if ok {
            //print("Carro \(carro.nome) salvo com sucesso.")
        }
    }
    
    
    func getPhotoArray(tipo: String) -> Array<Photo> {
        
        // AppDelegate da aplicação
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        // Context para salvar/deletar/buscar objetos
        let context = appDelegate.managedObjectContext
        
        // Define a entidade utilizada para a consulta
        let entity = NSEntityDescription.entityForName("Photo", inManagedObjectContext:context)
        
        // Cria a request com os filtros da consulta
        let request = NSFetchRequest()
        request.entity = entity
        
        // Buscar por tipo, where tipo=?
        //let query = NSPredicate(format:"tipo = " + tipo,nil)
        //request.predicate = query
        
        // Ordena a consulta por ‘timestamp’
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: true)
        let sortDescriptors = [sortDescriptor]
        request.sortDescriptors = sortDescriptors
        
        // Executa a consulta
        do {
            let array = try context.executeFetchRequest(request)
            return array as! Array<Photo>
            
        } catch {
            // Tratar erros aqui
            print("Erro \(error)")
            return [] as Array<Photo>;
        }
    }
}